import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { ResponseAPI } from 'src/app/shared/models/response';
import { ProfileUser } from '../headder/headder.model';

@Injectable({ providedIn: 'root' })
export class HttpService {
    constructor(private httpClient: HttpClient ) { }
    async callLogin( obj: object ){
        const res: ResponseAPI = await  this.httpClient.post<ResponseAPI>(environment.apiLogin, obj).toPromise();
        return res;
     }
     async callUsername( obj ){
     const res: ProfileUser = await this.httpClient.get<ProfileUser>(environment.apiUser, { params: obj }).toPromise();
     return res;
     }
}

