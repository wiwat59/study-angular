import { Component, OnInit } from '@angular/core';
import { RegisterForm } from '../register/model/register.model';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpService } from 'src/app/core/service/http-insert.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  constructor(private builder: FormBuilder, private http: HttpService) { 
  }
  regisform: RegisterForm;
  form: FormGroup;
  ngOnInit() {
    this.ValidateForm();
  }
  async Onsubmit() {
    // this.form.get('customerEmail').markAsTouched();
    // this.form.get('password').markAsTouched();
    // this.form.get('customerFullName').markAsTouched();
    // this.form.get('customerBirthDay').markAsTouched();
    // this.form.get('customerPhone').markAsTouched();
    // this.form.get('customerTaxId').markAsTouched();
    // if(!this.form_birtday.markAllAsTouched()){
    //   return;
    // }else{
    //   this.form.markAllAsTouched()
    // }
    if (this.form_birtday.invalid) {
      this.form_birtday.markAllAsTouched();
      return;
    }else if(this.form.invalid){
      this.form.markAllAsTouched();
      return;
    } else {
      const myJson: object =  this.form.value;
      const res = await this.http.InsertRegister(myJson);
      console.log(res);
    }
  }
  public ValidateForm() {
    this.form = this.builder.group({
      customerEmail: [null, [Validators.required,Validators.minLength(10)]],
      password: [null, Validators.required],
      customerFullName: [null, Validators.required],
      customerGender: ['m'],
      // customerBirthDay:[null, Validators.required],
      // customerPhone:[null, Validators.required],
      customerTaxId:[null, Validators.required],
    form_birtday: this.builder.group({
      customerBirthDay:[null, Validators.required],
      customerPhone:[null, [Validators.required, Validators.minLength(10), Validators.pattern("^-?[0-9]\\d*(\\.\\d{1,2})?$")]],
     })
    });
  }
  get form_birtday(): any { return this.form.get('form_birtday') as FormGroup; }
  get Email(): any { return this.form.get('customerEmail'); }
}
